terraform {
  backend "http" {
  }
}

provider "google" {
  credentials = file("creds.json")
  project     = var.gcp_project
  region      = "us-west1"
}

data "google_compute_network" "the_default" {
  name = var.gcp_network
}
resource "google_compute_subnetwork" "test-subnetwork" {
  name          = "l7-xlb-subnet"
  ip_cidr_range = "10.0.1.0/24"
  region        = "us-west1"
  network       = data.google_compute_network.the_default.id
  project       = var.gcp_project

}

# resource "google_compute_network" "custom-test" {
#   name                    = "l7-xlb-network"
#   auto_create_subnetworks = false
#   project                 = var.gcp_project

# }

# reserved IP address
resource "google_compute_global_address" "test_global_address" {
  name    = "l7-xlb-static-ip"
  project = var.gcp_project
}

# forwarding rule
resource "google_compute_global_forwarding_rule" "test_global_forwarding_rule" {
  name                  = "l7-xlb-forwarding-rule"
  ip_protocol           = "TCP"
  load_balancing_scheme = "EXTERNAL"
  port_range            = "80"
  target                = google_compute_target_http_proxy.test_http_proxy.id
  ip_address            = google_compute_global_address.test_global_address.id
  project               = var.gcp_project
}

# http proxy
resource "google_compute_target_http_proxy" "test_http_proxy" {
  name    = "l7-xlb-target-http-proxy"
  url_map = google_compute_url_map.test_url_map.id
  project = var.gcp_project
}

# url map
resource "google_compute_url_map" "test_url_map" {
  name            = "l7-xlb-url-map"
  default_service = google_compute_backend_service.test_backend.id
  project         = var.gcp_project
}

# backend service with custom request and response headers
resource "google_compute_backend_service" "test_backend" {
  name                    = "l7-xlb-backend-service"
  project                 = var.gcp_project
  protocol                = "HTTP"
  port_name               = "my-port"
  load_balancing_scheme   = "EXTERNAL"
  timeout_sec             = 10
  enable_cdn              = true
  custom_request_headers  = ["X-Client-Geo-Location: {client_region_subdivision}, {client_city}"]
  custom_response_headers = ["X-Cache-Hit: {cdn_cache_status}"]
  health_checks           = [google_compute_health_check.test_health_check.id]
  backend {
    group           = google_compute_instance_group.test_instance_group.id
    balancing_mode  = "UTILIZATION"
    capacity_scaler = 1.0
  }
}



# health check
resource "google_compute_health_check" "test_health_check" {
  name    = "l7-xlb-hc"
  project = var.gcp_project
  http_health_check {
    port_specification = "USE_SERVING_PORT"
  }
}


resource "google_compute_instance_group" "test_instance_group" {
  name        = "terraform-test"
  description = "Terraform test instance group"
  zone        = "us-west1-a"
  network     = data.google_compute_network.the_default.id
}

# allow access from health check ranges
resource "google_compute_firewall" "test_compute_firewall" {
  project       = var.gcp_project
  name          = "l7-xlb-fw-allow-hc"
  direction     = "INGRESS"
  network       = data.google_compute_network.the_default.id
  source_ranges = ["130.211.0.0/22", "35.191.0.0/16"]
  allow {
    protocol = "tcp"
  }
  target_tags = ["allow-health-check"]
}

resource "google_compute_router" "test_router" {
  name    = "test-router"
  region  = google_compute_subnetwork.test-subnetwork.region
  network = data.google_compute_network.the_default.id
}

resource "google_compute_router_nat" "nat" {
  name                               = "my-router-nat"
  router                             = google_compute_router.test_router.name
  region                             = google_compute_router.test_router.region
  nat_ip_allocate_option             = "AUTO_ONLY"
  source_subnetwork_ip_ranges_to_nat = "LIST_OF_SUBNETWORKS"
  subnetwork {
    name                    = google_compute_subnetwork.test-subnetwork.id
    source_ip_ranges_to_nat = ["ALL_IP_RANGES"]
  }

  log_config {
    enable = true
    filter = "ERRORS_ONLY"
  }
}

module "firewall_rules" {
  source       = "terraform-google-modules/network/google//modules/firewall-rules"
  project_id   = var.gcp_project
  network_name = data.google_compute_network.the_default.id

  rules = [{
    name                    = "allow-ssh-iap"
    description             = null
    direction               = "INGRESS"
    priority                = null
    ranges                  = ["35.235.240.0/20"]
    source_tags             = null
    source_service_accounts = null
    target_tags             = null
    target_service_accounts = null
    allow = [{
      protocol = "tcp"
      ports    = ["22"]
    }]
    deny = []
    log_config = {
      metadata = "INCLUDE_ALL_METADATA"
    }
  }]
}
